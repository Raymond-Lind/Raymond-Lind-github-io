<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Style-Type" content="text/css">
  <title></title>
  <meta name="Generator" content="Cocoa HTML Writer">
  <meta name="CocoaVersion" content="2299">
  <style type="text/css">
    p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Monaco; color: #ffffff; color: rgba(255, 255, 255, 0.6); -webkit-text-stroke: rgba(255, 255, 255, 0.6); background-color: #000000}
    p.p2 {margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Monaco; color: #ffffff; -webkit-text-stroke: #ffffff; background-color: #000000}
    p.p3 {margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Monaco; color: #14986a; -webkit-text-stroke: #14986a; background-color: #000000}
    p.p4 {margin: 0.0px 0.0px 0.0px 0.0px; font: 14.0px Monaco; color: #ffffff; -webkit-text-stroke: #ffffff; background-color: #000000; min-height: 19.0px}
    span.s1 {font-kerning: none}
    span.s2 {font-kerning: none; color: #2781c9; -webkit-text-stroke: 0px #2781c9}
    span.s3 {font-kerning: none; color: #14986a; -webkit-text-stroke: 0px #14986a}
    span.s4 {font-kerning: none; color: #eb102f; -webkit-text-stroke: 0px #eb102f}
    span.s5 {font-kerning: none; color: #ffffff; -webkit-text-stroke: 0px #ffffff}
    span.s6 {font-kerning: none; color: #d41466; -webkit-text-stroke: 0px #d41466}
  </style>
</head>
<body>
<p class="p1"><span class="s1">&lt;!DOCTYPE </span><span class="s2">html</span><span class="s1">&gt;</span></p>
<p class="p2"><span class="s1">&lt;html&gt;</span></p>
<p class="p2"><span class="s1">&lt;head&gt;</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">  </span>&lt;title&gt;Redirect&lt;/title&gt;</span></p>
<p class="p2"><span class="s1">&lt;/head&gt;</span></p>
<p class="p2"><span class="s1">&lt;body&gt;</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">  </span>&lt;script&gt;</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">    </span></span><span class="s2">const</span><span class="s1"> host = </span><span class="s3">'example.com'</span><span class="s1">;</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">    </span></span><span class="s2">const</span><span class="s1"> url = </span><span class="s3">'https://'</span><span class="s1"> + host + </span><span class="s3">'/path'</span><span class="s1">;</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">    </span></span><span class="s2">const</span><span class="s1"> headers = </span><span class="s2">new</span><span class="s1"> </span><span class="s4">Headers</span><span class="s1">({</span></p>
<p class="p3"><span class="s5"><span class="Apple-converted-space">      </span></span><span class="s1">'X-Forwarded-Host'</span><span class="s5">: host,</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">    </span>});</span></p>
<p class="p4"><span class="s1"></span><br></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">    </span></span><span class="s2">const</span><span class="s1"> request = </span><span class="s2">new</span><span class="s1"> </span><span class="s4">Request</span><span class="s1">(url, {</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">      </span></span><span class="s6">method</span><span class="s1">: </span><span class="s3">'GET'</span><span class="s1">,</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">      </span></span><span class="s6">headers</span><span class="s1">: headers,</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">      </span></span><span class="s6">mode</span><span class="s1">: </span><span class="s3">'no-cors'</span><span class="s1">,</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">      </span></span><span class="s6">redirect</span><span class="s1">: </span><span class="s3">'manual'</span><span class="s1">,</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">    </span>});</span></p>
<p class="p4"><span class="s1"></span><br></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">    </span></span><span class="s4">fetch</span><span class="s1">(request)</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">      </span>.</span><span class="s4">then</span><span class="s1">((response) =&gt; {</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">        </span></span><span class="s2">if</span><span class="s1"> (response.ok) {</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">          </span></span><span class="s6">window</span><span class="s1">.location.</span><span class="s4">replace</span><span class="s1">(response.url);</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">        </span>}</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">      </span>});</span></p>
<p class="p2"><span class="s1"><span class="Apple-converted-space">  </span>&lt;/script&gt;</span></p>
<p class="p2"><span class="s1">&lt;/body&gt;</span></p>
<p class="p2"><span class="s1">&lt;/html&gt;</span></p>
</body>
</html>
